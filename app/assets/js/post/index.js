import {createApp} from "vue";
import PostList from './post-list';
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueLoading from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/vue-loading.css';

const app = createApp(PostList);
app.use(VueAxios, axios);
app.use(VueLoading);
app.mount('#root');
