import {createApp} from "vue";
import RepairList from './repair-list';
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueLoading from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/vue-loading.css';
import vueDebounce from 'vue-debounce'

const app = createApp(RepairList);

app.use(VueAxios, axios);
app.use(VueLoading);
app.use(vueDebounce);
app.mount('#root');