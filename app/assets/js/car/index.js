import {createApp} from "vue";
import CarList from './car-list';
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueLoading from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/vue-loading.css';
import vueDebounce from "vue-debounce";

const app = createApp(CarList);
app.use(VueLoading);
app.use(vueDebounce);
app.use(VueAxios, axios);
app.mount('#root');