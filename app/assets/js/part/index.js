import {createApp} from "vue";
import PartList from './part-list';
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueLoading from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/vue-loading.css';
import vueDebounce from "vue-debounce";

const app = createApp(PartList);
app.use(VueAxios, axios);
app.use(VueLoading);
app.use(vueDebounce);
app.mount('#root');