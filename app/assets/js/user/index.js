import {createApp} from "vue";
import List from './list';
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueLoading from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/vue-loading.css';
import vueDebounce from "vue-debounce";

const app = createApp(List);
app.use(VueAxios, axios);
app.use(VueLoading);
app.use(vueDebounce);
app.mount('#root');