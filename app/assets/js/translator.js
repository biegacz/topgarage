import Translator from 'bazinga-translator';

export function translate(key, params, domain) {
    return Translator.trans(key, params, domain);
}