const Encore = require('@symfony/webpack-encore');

if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

Encore
    .setOutputPath('public/build/')
    .setPublicPath('/build')
    .enableVueLoader(() => {}, { runtimeCompilerBuild: true, version: 3 })
    .addEntry('app', './assets/app.js')
    .addEntry('user-index', './assets/js/user/index.js')
    .addEntry('client-index', './assets/js/client/index.js')
    .addEntry('part-index', './assets/js/part/index.js')
    .addEntry('car-index', './assets/js/car/index.js')
    .addEntry('repair-index', './assets/js/repair/index.js')
    .addEntry('post-index', './assets/js/post/index.js')

    // When enabled, Webpack "splits" your files into smaller pieces for greater optimization.
    .splitEntryChunks()

    .enableSingleRuntimeChunk()

    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    // enables hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())

    .configureBabel((config) => {
        config.plugins.push('@babel/plugin-proposal-class-properties');
    })
    .configureDevServerOptions(options => {
        options.allowedHosts = 'all';
    })

    // enables @babel/preset-env polyfills
    .configureBabelPresetEnv((config) => {
        config.useBuiltIns = 'usage';
        config.corejs = 3;
    })

    .enableSassLoader()

    // uncomment if you're having problems with a jQuery plugin
    //.autoProvidejQuery()
;

module.exports = Encore.getWebpackConfig();
