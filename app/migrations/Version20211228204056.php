<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211228204056 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE car_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE client_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE part_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE repair_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE repair_part_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE car (id INT NOT NULL, owner_id INT NOT NULL, model VARCHAR(255) NOT NULL, manufacturer VARCHAR(255) NOT NULL, vin VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_773DE69D7E3C61F9 ON car (owner_id)');
        $this->addSql('COMMENT ON COLUMN car.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE client (id INT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE part (id INT NOT NULL, name VARCHAR(255) NOT NULL, quantity INT NOT NULL, manufacturer VARCHAR(255) DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN part.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE repair (id INT NOT NULL, car_id INT NOT NULL, description TEXT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_8EE43421C3C6F69F ON repair (car_id)');
        $this->addSql('COMMENT ON COLUMN repair.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE repair_user (repair_id INT NOT NULL, user_id INT NOT NULL, PRIMARY KEY(repair_id, user_id))');
        $this->addSql('CREATE INDEX IDX_58B502A943833CFF ON repair_user (repair_id)');
        $this->addSql('CREATE INDEX IDX_58B502A9A76ED395 ON repair_user (user_id)');
        $this->addSql('CREATE TABLE repair_part (id INT NOT NULL, part_id INT NOT NULL, repair_id INT NOT NULL, quantity INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_9C29A4264CE34BEC ON repair_part (part_id)');
        $this->addSql('CREATE INDEX IDX_9C29A42643833CFF ON repair_part (repair_id)');
        $this->addSql('COMMENT ON COLUMN repair_part.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE car ADD CONSTRAINT FK_773DE69D7E3C61F9 FOREIGN KEY (owner_id) REFERENCES client (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE repair ADD CONSTRAINT FK_8EE43421C3C6F69F FOREIGN KEY (car_id) REFERENCES car (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE repair_user ADD CONSTRAINT FK_58B502A943833CFF FOREIGN KEY (repair_id) REFERENCES repair (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE repair_user ADD CONSTRAINT FK_58B502A9A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE repair_part ADD CONSTRAINT FK_9C29A4264CE34BEC FOREIGN KEY (part_id) REFERENCES part (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE repair_part ADD CONSTRAINT FK_9C29A42643833CFF FOREIGN KEY (repair_id) REFERENCES repair (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
//        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE repair DROP CONSTRAINT FK_8EE43421C3C6F69F');
        $this->addSql('ALTER TABLE car DROP CONSTRAINT FK_773DE69D7E3C61F9');
        $this->addSql('ALTER TABLE repair_part DROP CONSTRAINT FK_9C29A4264CE34BEC');
        $this->addSql('ALTER TABLE repair_user DROP CONSTRAINT FK_58B502A943833CFF');
        $this->addSql('ALTER TABLE repair_part DROP CONSTRAINT FK_9C29A42643833CFF');
        $this->addSql('DROP SEQUENCE car_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE client_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE part_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE repair_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE repair_part_id_seq CASCADE');
        $this->addSql('DROP TABLE car');
        $this->addSql('DROP TABLE client');
        $this->addSql('DROP TABLE part');
        $this->addSql('DROP TABLE repair');
        $this->addSql('DROP TABLE repair_user');
        $this->addSql('DROP TABLE repair_part');
    }
}
