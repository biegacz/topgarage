<?php

namespace App\Command;

use App\Entity\User;
use App\Repository\UserRepository;
use Assert\Assert;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppInitCommand extends Command
{
    protected static $defaultName = 'app:init';
    private UserRepository $userRepository;
    private UserPasswordHasherInterface $hasher;

    public function __construct(
        string $name = null,
        UserRepository $userRepository,
        UserPasswordHasherInterface $hasher
    ) {
        parent::__construct($name);
        $this->userRepository = $userRepository;
        $this->hasher = $hasher;
    }

    protected function configure()
    {
        $this
            ->setDescription('Init application after first time install (no need for fixtures)')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $io->text('Attempting to create user');

        $email = $io->ask('Provide new/existing user email', 'admin@local.com');
        $existingUser = $this->userRepository->findOneByEmail($email);

        $name = $io->ask('Provide user name', $existingUser ? $existingUser->getName() : 'User');
        $pass = $io->askHidden('Provide new user password (default to root if no password provided)', function (
            $password
        ) {
            if (empty($password)) {
                return 'root';
            }

            return $password;
        });

        $email ??= 'admin@local.com';

        Assert::that($email)->email();
        Assert::that($pass)->notNull();

        if (null === $existingUser) {
            $user = new User();
            $user->setName($name);
            $user->setEmail($email);
            $user->setPassword($this->hasher->hashPassword($user, $pass));
            $user->setRoles(['ROLE_SUPER_ADMIN']);
            $this->userRepository->save($user);

            $io->success(sprintf('Successfully created user %s', $email));
        } else {
            $existingUser->setPassword($this->hasher->hashPassword($existingUser, $pass));
            $existingUser->setName($name);
            $this->userRepository->save($existingUser);
            $io->warning(sprintf('User %s exists, updating password and name.', $email));
        }

        return 0;
    }
}
