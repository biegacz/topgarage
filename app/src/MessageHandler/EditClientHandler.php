<?php

namespace App\MessageHandler;

use App\Message\EditClient;
use App\Repository\ClientRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class EditClientHandler implements MessageHandlerInterface
{
    private ClientRepository $clientRepository;

    public function __construct(ClientRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    public function __invoke(EditClient $message)
    {
        $client = $this->clientRepository->getById($message->getId());

        $client->setName($message->getName());
        $client->setEmail($message->getEmail());
        $client->setPhone($message->getPhone());

        $this->clientRepository->save($client);
    }
}
