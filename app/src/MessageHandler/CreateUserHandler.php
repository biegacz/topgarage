<?php

namespace App\MessageHandler;

use App\Entity\User;
use App\Message\CreateUser;
use App\Repository\UserRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

final class CreateUserHandler implements MessageHandlerInterface
{
    private UserRepository $userRepository;
    private UserPasswordHasherInterface $hasher;

    public function __construct(UserRepository $userRepository, UserPasswordHasherInterface $hasher)
    {
        $this->userRepository = $userRepository;
        $this->hasher = $hasher;
    }

    public function __invoke(CreateUser $message): void
    {
        $user = new User();
        $user->setName($message->getName());
        $user->setEmail($message->getEmail());
        $user->setPassword($this->hasher->hashPassword($user, $message->getPassword()));
        $user->setRoles($message->getRoles());
        $user->setCreatedAt(new \DateTimeImmutable());

        $this->userRepository->save($user);
    }
}
