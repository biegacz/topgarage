<?php

namespace App\MessageHandler;

use App\Entity\Client;
use App\Message\CreateClient;
use App\Repository\ClientRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class CreateClientHandler implements MessageHandlerInterface
{
    private ClientRepository $clientRepository;

    public function __construct(ClientRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    public function __invoke(CreateClient $message)
    {
        $client = new Client();
        $client->setName($message->getName());
        $client->setEmail($message->getEmail());
        $client->setPhone($message->getPhone());
        $client->setCreatedAt(new \DateTimeImmutable());

        $this->clientRepository->save($client);
    }
}
