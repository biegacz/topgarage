<?php

namespace App\MessageHandler;

use App\Message\EditVisit;
use App\Repository\VisitRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class EditVisitHandler implements MessageHandlerInterface
{
    private VisitRepository $visitRepository;

    public function __construct(VisitRepository $visitRepository)
    {
        $this->visitRepository = $visitRepository;
    }

    public function __invoke(EditVisit $message): void
    {
        $visit = $this->visitRepository->getById($message->getId());
        $visit->setDescription($message->getDescription());
        $visit->setDate(new \DateTimeImmutable($message->getDate()));

        $this->visitRepository->save($visit);
    }
}
