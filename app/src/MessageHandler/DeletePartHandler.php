<?php

namespace App\MessageHandler;

use App\Message\DeletePart;
use App\Repository\PartRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class DeletePartHandler implements MessageHandlerInterface
{
    private PartRepository $partRepository;

    public function __construct(PartRepository $partRepository)
    {
        $this->partRepository = $partRepository;
    }

    public function __invoke(DeletePart $message): void
    {
        $this->partRepository->remove($message->getId());
    }
}
