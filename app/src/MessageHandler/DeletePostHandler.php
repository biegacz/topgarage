<?php

namespace App\MessageHandler;

use App\Message\DeletePost;
use App\Repository\PostRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class DeletePostHandler implements MessageHandlerInterface
{
    private PostRepository $postRepository;

    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    public function __invoke(DeletePost $message): void
    {
        $this->postRepository->remove($message->getId());
    }
}
