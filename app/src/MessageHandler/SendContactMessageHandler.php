<?php

namespace App\MessageHandler;

use App\Message\SendContactMessage;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

final class SendContactMessageHandler implements MessageHandlerInterface
{
    private TranslatorInterface $translator;
    private MailerInterface $mailer;
    private string $to;

    public function __construct(string $to, TranslatorInterface $translator, MailerInterface $mailer)
    {
        $this->translator = $translator;
        $this->mailer = $mailer;
        $this->to = $to;
    }

    public function __invoke(SendContactMessage $message)
    {
        $email = (new TemplatedEmail())
            ->to($this->to)
            ->subject($this->translator->trans('contact_form_subject'))
            ->htmlTemplate('email/contact.html.twig')
            ->replyTo($message->getEmail())
            ->context([
                'name' => $message->getName(),
                'message' => $message->getMessage(),
                'sender_email' => $message->getEmail(),
            ]);

        $this->mailer->send($email);
    }
}
