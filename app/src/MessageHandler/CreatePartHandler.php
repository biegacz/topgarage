<?php

namespace App\MessageHandler;

use App\Entity\Part;
use App\Message\CreatePart;
use App\Repository\PartRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class CreatePartHandler implements MessageHandlerInterface
{
    private PartRepository $partRepository;

    public function __construct(PartRepository $partRepository)
    {
        $this->partRepository = $partRepository;
    }

    public function __invoke(CreatePart $message): void
    {
        $part = new Part();
        $part->setManufacturer($message->getManufacturer());
        $part->setName($message->getName());
        $part->setQuantity($message->getQuantity());
        $part->setCreatedAt(new \DateTimeImmutable());

        $this->partRepository->save($part);
    }
}
