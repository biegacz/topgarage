<?php

namespace App\MessageHandler;

use App\Entity\Repair;
use App\Entity\RepairPart;
use App\Message\CreateRepair;
use App\Repository\CarRepository;
use App\Repository\PartRepository;
use App\Repository\RepairRepository;
use App\Repository\UserRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class CreateRepairHandler implements MessageHandlerInterface
{
    private UserRepository $userRepository;
    private CarRepository $carRepository;
    private RepairRepository $repairRepository;
    private PartRepository $partRepository;

    public function __construct(
        UserRepository $userRepository,
        CarRepository $carRepository,
        RepairRepository $repairRepository,
        PartRepository $partRepository
    ) {
        $this->userRepository = $userRepository;
        $this->carRepository = $carRepository;
        $this->repairRepository = $repairRepository;
        $this->partRepository = $partRepository;
    }

    public function __invoke(CreateRepair $message): void
    {
        $repair = new Repair();

        $car = $this->carRepository->getById($message->getCarId());

        $repair->setDescription($message->getDescription());
        $repair->setCreatedAt(new \DateTimeImmutable());
        $repair->setCar($car);

        foreach ($message->getRepairmen() as $repairmanId) {
            $repairman = $this->userRepository->getById($repairmanId);
            $repair->addRepairman($repairman);
        }

        foreach ($message->getParts() as $partData) {
            $partId = $partData['id'];
            $partQuantity = $partData['quantity'];

            $part = $this->partRepository->getById($partId);
            $repairPart = new RepairPart();
            $repairPart->setRepair($repair);
            $repairPart->setPart($part);
            $repairPart->setCreatedAt(new \DateTimeImmutable());
            $repairPart->setQuantity($partQuantity);
            $repair->addRepairPart($repairPart);
        }

        $this->repairRepository->save($repair);
    }
}
