<?php

namespace App\MessageHandler;

use App\Message\DeleteClient;
use App\Repository\ClientRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class DeleteClientHandler implements MessageHandlerInterface
{
    private ClientRepository $clientRepository;

    public function __construct(ClientRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    public function __invoke(DeleteClient $message): void
    {
        $this->clientRepository->remove($message->getId());
    }
}
