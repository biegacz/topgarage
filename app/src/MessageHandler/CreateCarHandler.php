<?php

namespace App\MessageHandler;

use App\Entity\Car;
use App\Message\CreateCar;
use App\Repository\CarRepository;
use App\Repository\ClientRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class CreateCarHandler implements MessageHandlerInterface
{
    private ClientRepository $clientRepository;
    private CarRepository $carRepository;

    public function __construct(ClientRepository $clientRepository, CarRepository $carRepository)
    {
        $this->clientRepository = $clientRepository;
        $this->carRepository = $carRepository;
    }

    public function __invoke(CreateCar $message): void
    {
        $client = $this->clientRepository->getById($message->getClient());

        $car = new Car();
        $car->setModel($message->getModel());
        $car->setManufacturer($message->manufacturer());
        $car->setVin($message->vin());
        $car->setOwner($client);
        $car->setCreatedAt(new \DateTimeImmutable());

        $this->carRepository->save($car);
    }
}
