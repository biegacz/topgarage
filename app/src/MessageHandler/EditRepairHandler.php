<?php

namespace App\MessageHandler;

use App\Entity\RepairPart;
use App\Entity\User;
use App\Message\EditRepair;
use App\Repository\CarRepository;
use App\Repository\PartRepository;
use App\Repository\RepairRepository;
use App\Repository\UserRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class EditRepairHandler implements MessageHandlerInterface
{
    private RepairRepository $repairRepository;
    private CarRepository $carRepository;
    private PartRepository $partRepository;
    private UserRepository $userRepository;

    public function __construct(
        RepairRepository $repairRepository,
        CarRepository $carRepository,
        PartRepository $partRepository,
        UserRepository $userRepository
    ) {
        $this->repairRepository = $repairRepository;
        $this->carRepository = $carRepository;
        $this->partRepository = $partRepository;
        $this->userRepository = $userRepository;
    }

    public function __invoke(EditRepair $message)
    {
        $repair = $this->repairRepository->getById($message->getId());
        $car = $this->carRepository->getById($message->getCarId());

        $repair->setCar($car);
        $repair->setDescription($message->getDescription());

        $repairmen = $message->getRepairmen();
        $existingRepairmenIds = $repair->getRepairmen()->map(fn(User $user) => $user->getId())->toArray();
        $repairmenToRemove = array_diff($existingRepairmenIds, $repairmen);
        $repairmenToAdd = array_diff($repairmen, $existingRepairmenIds);

        foreach ($repairmenToAdd as $repairmanId) {
            $repairman = $this->userRepository->getById($repairmanId);
            $repair->addRepairman($repairman);
        }

        foreach ($repairmenToRemove as $repairmanId) {
            $repair->removeRepairmanById($repairmanId);
        }

        $newPartIds = array_column($message->getParts(), 'id');
        $existingPartIds = $repair
            ->getRepairParts()
            ->map(fn(RepairPart $repairPart) => $repairPart->getPart()->getId())
            ->toArray();
        $partToAddIds = array_values(array_diff($newPartIds, $existingPartIds));
        $partsMap = array_column($message->getParts(), null, 'id');

        /** @var RepairPart $repairPart */
        foreach ($repair->getRepairParts() as $repairPart) {
            $sourcePart = $repairPart->getPart();
            if (false === in_array($sourcePart->getId(), $newPartIds)) {
                $repair->removeRepairPart($repairPart);
            } else {
                $repairPart->setQuantity($partsMap[$sourcePart->getId()]['quantity']);
            }
        }

        foreach ($partToAddIds as $id) {
            $partData = $partsMap[$id];

            $partId = $partData['id'];
            $partQuantity = $partData['quantity'];

            $part = $this->partRepository->getById($partId);
            $repairPart = new RepairPart();
            $repairPart->setRepair($repair);
            $repairPart->setPart($part);
            $repairPart->setCreatedAt(new \DateTimeImmutable());
            $repairPart->setQuantity($partQuantity);
            $repair->addRepairPart($repairPart);
        }

        $this->repairRepository->save($repair);
    }
}
