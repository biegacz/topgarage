<?php

namespace App\MessageHandler;

use App\Message\DeleteVisit;
use App\Repository\VisitRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class DeleteVisitHandler implements MessageHandlerInterface
{
    private VisitRepository $visitRepository;

    public function __construct(VisitRepository $visitRepository)
    {
        $this->visitRepository = $visitRepository;
    }

    public function __invoke(DeleteVisit $message): void
    {
        $this->visitRepository->remove($message->getId());
    }
}
