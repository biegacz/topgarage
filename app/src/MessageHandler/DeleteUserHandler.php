<?php

namespace App\MessageHandler;

use App\Message\DeleteUser;
use App\Repository\UserRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class DeleteUserHandler implements MessageHandlerInterface
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function __invoke(DeleteUser $message): void
    {
        $this->userRepository->remove($message->getId());
    }
}
