<?php

namespace App\MessageHandler;

use App\Entity\Post;
use App\Message\CreatePost;
use App\Repository\PostRepository;
use App\Repository\UserRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class CreatePostHandler implements MessageHandlerInterface
{
    private PostRepository $postRepository;
    private UserRepository $userRepository;

    public function __construct(PostRepository $postRepository, UserRepository $userRepository)
    {
        $this->postRepository = $postRepository;
        $this->userRepository = $userRepository;
    }

    public function __invoke(CreatePost $message)
    {
        $user = $this->userRepository->getById($message->getCreatedById());

        $post = new Post();
        $post->setTitle($message->getTitle());
        $post->setContent($message->getContent());
        $post->setCreatedBy($user);
        $post->setCreatedAt(new \DateTimeImmutable());

        $this->postRepository->save($post);
    }
}
