<?php

namespace App\MessageHandler;

use App\Message\EditPost;
use App\Repository\PostRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class EditPostHandler implements MessageHandlerInterface
{
    private PostRepository $postRepository;

    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    public function __invoke(EditPost $message): void
    {
        $post = $this->postRepository->getById($message->getId());
        $post->setContent($message->getContent());
        $post->setTitle($message->getTitle());

        $this->postRepository->save($post);
    }
}
