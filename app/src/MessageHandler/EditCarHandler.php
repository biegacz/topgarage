<?php

namespace App\MessageHandler;

use App\Message\EditCar;
use App\Repository\CarRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class EditCarHandler implements MessageHandlerInterface
{
    private CarRepository $carRepository;

    public function __construct(CarRepository $carRepository)
    {
        $this->carRepository = $carRepository;
    }

    public function __invoke(EditCar $message): void
    {
        $car = $this->carRepository->getById($message->getId());

        $car->setModel($message->getModel());
        $car->setManufacturer($message->manufacturer());
        $car->setVin($message->vin());

        $this->carRepository->save($car);
    }
}
