<?php

namespace App\MessageHandler;

use App\Message\UserPartForRepair;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class UserPartForRepairHandler implements MessageHandlerInterface
{
    public function __invoke(UserPartForRepair $message)
    {
        // do something with your message
    }
}
