<?php

namespace App\MessageHandler;

use App\Message\DeleteRepair;
use App\Repository\RepairRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class DeleteRepairHandler implements MessageHandlerInterface
{
    private RepairRepository $repairRepository;

    public function __construct(RepairRepository $repairRepository)
    {
        $this->repairRepository = $repairRepository;
    }

    public function __invoke(DeleteRepair $message): void
    {
        $this->repairRepository->remove($message->getId());
    }
}
