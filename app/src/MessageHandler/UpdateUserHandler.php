<?php

namespace App\MessageHandler;

use App\Message\UpdateUser;
use App\Repository\UserRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class UpdateUserHandler implements MessageHandlerInterface
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function __invoke(UpdateUser $message): void
    {
        $user = $this->userRepository->getById($message->getId());

        $user->setName($message->getName());
        $user->setEmail($message->getEmail());
        $user->setRoles($message->getRoles());

        $this->userRepository->save($user);
    }
}
