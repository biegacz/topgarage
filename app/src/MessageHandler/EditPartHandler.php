<?php

namespace App\MessageHandler;

use App\Message\EditPart;
use App\Repository\PartRepository;
use App\Service\PartQuerySrv;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class EditPartHandler implements MessageHandlerInterface
{
    private PartRepository $partRepository;
    private PartQuerySrv $partQuerySrv;

    public function __construct(PartRepository $partRepository, PartQuerySrv $partQuerySrv)
    {
        $this->partRepository = $partRepository;
        $this->partQuerySrv = $partQuerySrv;
    }

    public function __invoke(EditPart $message)
    {
        $part = $this->partRepository->getById($message->getId());

        $usage = $this->partQuerySrv->getPartUsage($part);
        if (($message->getQuantity() - $usage) < 0) {
            throw new \LogicException('Cannot set quantity less then used parts');
        }

        $part->setQuantity($message->getQuantity());
        $part->setManufacturer($message->getManufacturer());
        $part->setName($message->getName());

        $this->partRepository->save($part);
    }
}
