<?php

namespace App\MessageHandler;

use App\Entity\Visit;
use App\Message\CreateVisit;
use App\Repository\ClientRepository;
use App\Repository\VisitRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class CreateVisitHandler implements MessageHandlerInterface
{
    private VisitRepository $visitRepository;
    private ClientRepository $clientRepository;

    public function __construct(VisitRepository $visitRepository, ClientRepository $clientRepository)
    {
        $this->visitRepository = $visitRepository;
        $this->clientRepository = $clientRepository;
    }

    public function __invoke(CreateVisit $message): void
    {
        $client = $this->clientRepository->getById($message->getClient());
        $visit = new Visit();
        $visit->setClient($client);
        $visit->setCreatedAt(new \DateTimeImmutable());
        $visit->setDate(new \DateTimeImmutable($message->getDate()));
        $visit->setDescription($message->getDescription());

        $this->visitRepository->save($visit);
    }
}
