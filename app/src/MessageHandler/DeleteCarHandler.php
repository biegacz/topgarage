<?php

namespace App\MessageHandler;

use App\Message\DeleteCar;
use App\Repository\CarRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class DeleteCarHandler implements MessageHandlerInterface
{
    private CarRepository $carRepository;

    public function __construct(CarRepository $carRepository)
    {
        $this->carRepository = $carRepository;
    }

    public function __invoke(DeleteCar $message)
    {
        $this->carRepository->remove($message->getId());
    }
}
