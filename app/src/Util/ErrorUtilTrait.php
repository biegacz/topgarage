<?php

namespace App\Util;

use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class ErrorUtilTrait
{
    public static function getFromViolationList(ConstraintViolationListInterface $violationList)
    {
        $errors = [];
        /** @var ConstraintViolation $error */
        foreach ($violationList as $error) {
            $errors[$error->getPropertyPath()][] = $error->getMessage();
        }
        return $errors;
    }
}