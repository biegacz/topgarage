<?php

namespace App\Message;

use Symfony\Component\Validator\Constraints as Assert;

final class CreateRepair
{
    #[Assert\NotBlank, Assert\Length(max: 2000)]
    private string $description;

    #[Assert\NotNull]
    private ?int $carId;

    private array $repairmen;
    private array $parts;

    public function __construct(string $description, ?int $carId, array $repairmen, array $parts)
    {
        $this->description = $description;
        $this->carId = $carId;
        $this->repairmen = $repairmen;
        $this->parts = $parts;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getCarId(): ?int
    {
        return $this->carId;
    }

    public function getRepairmen(): array
    {
        return $this->repairmen;
    }

    public function getParts(): array
    {
        return $this->parts;
    }
}