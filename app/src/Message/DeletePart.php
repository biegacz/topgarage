<?php

namespace App\Message;

final class DeletePart
{
    private int $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return  $this->id;
    }
}
