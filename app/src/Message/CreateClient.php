<?php

namespace App\Message;

use Symfony\Component\Validator\Constraints as Assert;

final class CreateClient
{
    #[Assert\NotBlank]
    private $name;
    private string $email;
    private string $phone;

    public function __construct(string $name, string $email, string $phone)
     {
         $this->name = $name;
         $this->email = $email;
         $this->phone = $phone;
     }

    public function getName(): string
    {
        return $this->name;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }
}
