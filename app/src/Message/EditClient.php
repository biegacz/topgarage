<?php

namespace App\Message;

use Symfony\Component\Validator\Constraints as Assert;

final class EditClient
{
    #[Assert\NotBlank]
    private $name;
    private int $id;
    private string $email;
    private string $phone;

    public function __construct(
        int $id,
        string $name,
        string $email,
        string $phone
    ) {
        $this->name = $name;
        $this->id = $id;
        $this->email = $email;
        $this->phone = $phone;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }
}
