<?php

namespace App\Message;

use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\User;

final class CreateUser
{
    #[Assert\NotBlank]
    private string $name;

    #[Assert\NotBlank, Assert\Email]
    private string $email;

    #[Assert\NotBlank]
    private string $password;

    #[Assert\Count(min: 1), Assert\Choice(User::AVAILABLE_ROLES, multiple: true)]
    private array $roles;

    public function __construct(string $name, string $email, string $password, array $roles)
    {
        $this->name = $name;
        $this->email = $email;
        $this->password = $password;
        $this->roles = $roles;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getRoles(): array
    {
        return $this->roles;
    }
}
