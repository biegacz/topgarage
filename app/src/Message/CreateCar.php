<?php

namespace App\Message;

final class CreateCar
{
    private string $model;
    private string $manufacturer;
    private string $vin;
    private int $client;

    public function __construct(string $model, string $manufacturer, string $vin, int $client)
     {
         $this->model = $model;
         $this->manufacturer = $manufacturer;
         $this->vin = $vin;
         $this->client = $client;
     }

    public function getModel(): string
    {
        return $this->model;
    }

    public function manufacturer(): string
    {
        return $this->manufacturer;
    }

    public function vin(): string
    {
        return $this->vin;
    }

    public function getClient(): int
    {
        return $this->client;
    }
}
