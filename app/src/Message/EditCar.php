<?php

namespace App\Message;

final class EditCar
{
    private int $id;
    private string $model;
    private string $manufacturer;
    private string $vin;

    public function __construct(int $id, string $model, string $manufacturer, string $vin)
    {
        $this->id = $id;
        $this->model = $model;
        $this->manufacturer = $manufacturer;
        $this->vin = $vin;
    }

    public function getModel(): string
    {
        return $this->model;
    }

    public function manufacturer(): string
    {
        return $this->manufacturer;
    }

    public function vin(): string
    {
        return $this->vin;
    }

    public function getId(): int
    {
        return $this->id;
    }
}
