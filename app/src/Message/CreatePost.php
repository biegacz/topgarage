<?php

namespace App\Message;

use Symfony\Component\Validator\Constraints as Assert;

final class CreatePost
{
    #[Assert\NotBlank, Assert\Length(max: 1000)]
    private string $title;

    #[Assert\NotBlank]
    private string $content;
    private int $createdById;

    public function __construct(string $title, string $content, int $createdById)
     {
         $this->title = $title;
         $this->content = $content;
         $this->createdById = $createdById;
     }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function getCreatedById(): int
    {
        return $this->createdById;
    }
}
