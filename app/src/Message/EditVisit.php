<?php

namespace App\Message;

final class EditVisit
{
    private int $id;
    private string $description;
    private string $date;

    public function __construct(
        int $id,
        string $description,
        string $date
    ) {
        $this->description = $description;
        $this->date = $date;
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getDate(): string
    {
        return $this->date;
    }

    public function getDescription(): string
    {
        return  $this->description;
    }
}
