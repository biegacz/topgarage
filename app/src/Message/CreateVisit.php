<?php

namespace App\Message;

final class CreateVisit
{
    private string $description;
    private string $date;
    private int $client;

    public function __construct(
        string $description,
        string $date,
        int $client
    ) {
        $this->description = $description;
        $this->date = $date;
        $this->client = $client;
    }

    public function getClient(): int
    {
        return $this->client;
    }

    public function getDate(): string
    {
        return $this->date;
    }

    public function getDescription(): string
    {
        return  $this->description;
    }
}
