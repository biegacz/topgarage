<?php

namespace App\Message;

use Symfony\Component\Validator\Constraints as Assert;

final class EditPost
{
    private int $id;

    #[Assert\NotBlank, Assert\Length(max: 1000)]
    private string $title;

    #[Assert\NotBlank]
    private string $content;

    public function __construct(int $id, string $title, string $content)
    {
        $this->title = $title;
        $this->content = $content;
        $this->id = $id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function getId(): int
    {
        return $this->id;
    }
}
