<?php

namespace App\Message;

use Symfony\Component\Validator\Constraints as Assert;

final class EditRepair
{
    #[Assert\NotBlank]
    private string $description;
    private int $id;

    #[Assert\NotNull]
    private int $carId;
    private array $repairmen;
    private array $parts;

    public function __construct(int $id, string $description, int $carId, array $repairmen, array $parts)
    {
        $this->id = $id;
        $this->description = $description;
        $this->carId = $carId;
        $this->repairmen = $repairmen;
        $this->parts = $parts;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getCarId(): int
    {
        return $this->carId;
    }

    public function getRepairmen(): array
    {
        return $this->repairmen;
    }

    public function getParts(): array
    {
        return $this->parts;
    }
}
