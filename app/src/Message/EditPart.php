<?php

namespace App\Message;

use Symfony\Component\Validator\Constraints as Assert;

final class EditPart
{
    #[Assert\NotBlank]
    private string $name;

    #[Assert\NotBlank]
    private string $manufacturer;

    #[Assert\NotBlank, Assert\GreaterThan(0)]
    private int $quantity;

    private int $id;

    public function __construct(
        int $id,
        string $name,
        string $manufacturer,
        int $quantity
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->manufacturer = $manufacturer;
        $this->quantity = $quantity;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getManufacturer(): string
    {
        return $this->manufacturer;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
