<?php

namespace App\Message;

use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\User;

final class UpdateUser
{
    private int $id;

    #[Assert\NotBlank]
    private string $name;

    #[Assert\NotBlank, Assert\Email]
    private string $email;

    #[Assert\Count(min: 1), Assert\Choice(User::AVAILABLE_ROLES, multiple: true)]
    private array $roles;

    public function __construct(int $id, string $name, string $email, array $roles)
    {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
        $this->roles = $roles;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getRoles(): array
    {
        return $this->roles;
    }
}
