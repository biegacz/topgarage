<?php

namespace App\Message;

use Symfony\Component\Validator\Constraints as Assert;

final class CreatePart
{
    #[Assert\NotBlank]
    private string $name;

    #[Assert\NotBlank]
    private string $manufacturer;

    #[Assert\NotBlank, Assert\GreaterThan(1)]
    private int $quantity;

    public function __construct(
        string $name,
        string $manufacturer,
        int $quantity
    ) {
        $this->name = $name;
        $this->manufacturer = $manufacturer;
        $this->quantity = $quantity;
    }

    public function getManufacturer(): string
    {
        return $this->manufacturer;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
