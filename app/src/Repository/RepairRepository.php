<?php

namespace App\Repository;

use App\Entity\Repair;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Repair|null find($id, $lockMode = null, $lockVersion = null)
 * @method Repair|null findOneBy(array $criteria, array $orderBy = null)
 * @method Repair[]    findAll()
 * @method Repair[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RepairRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Repair::class);
    }

    public function getAll(): array
    {
        return $this->findAll();
    }

    public function findByDescription(string $description): array
    {
        return $this->createQueryBuilder('r')
            ->where('LOWER(r.description) LIKE LOWER(:description)')
            ->setParameter('description', "%$description%")
            ->getQuery()->getResult();
    }

    public function getById(int $id): Repair
    {
        return $this->find($id);
    }

    public function remove(int $id): void
    {
        $entity = $this->getById($id);

        $this->getEntityManager()->remove($entity);
        $this->getEntityManager()->flush();
    }

    public function save(Repair $repair): void
    {
        $this->getEntityManager()->persist($repair);
        $this->getEntityManager()->flush();
    }
}
