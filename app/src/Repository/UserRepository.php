<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(PasswordAuthenticatedUserInterface $user, string $newHashedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newHashedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    public function findOneByEmail(string $email): ?User
    {
        return $this->findOneBy(['email' => $email]);
    }

    public function save(User $user): void
    {
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush();
    }

    public function findAllByRole(string $role): array
    {
        return $this->createQueryBuilder('u')
            ->where('TEXT(u.roles) like :role')
            ->setParameter('role', "%$role%")
            ->getQuery()->getResult();
    }

    public function getById(int $id): User
    {
        return $this->find($id);
    }

    public function remove(int $id): void
    {
        $entity = $this->getById($id);

        $this->getEntityManager()->remove($entity);
        $this->getEntityManager()->flush();
    }

    public function findByNameOrEmail(string $nameOrEmail): array
    {
        return $this->createQueryBuilder('u')
            ->where('LOWER(u.name) LIKE LOWER(:nameOrEmail) ')
            ->orWhere('LOWER(u.email) LIKE LOWER(:nameOrEmail) ')
            ->setParameter('nameOrEmail', "%$nameOrEmail%")
            ->orderBy('u.email', 'ASC')
            ->getQuery()->getResult();
    }
}
