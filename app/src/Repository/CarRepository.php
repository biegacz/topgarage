<?php

namespace App\Repository;

use App\Entity\Car;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Car|null find($id, $lockMode = null, $lockVersion = null)
 * @method Car|null findOneBy(array $criteria, array $orderBy = null)
 * @method Car[]    findAll()
 * @method Car[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CarRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Car::class);
    }

    public function getAll(): array
    {
        return $this->findAll();
    }

    public function save(Car $car): void
    {
        $this->getEntityManager()->persist($car);
        $this->getEntityManager()->flush();
    }

    public function getById(int $id): Car
    {
        return $this->find($id);
    }

    public function remove(int $id): void
    {
        $entity = $this->getById($id);

        $this->getEntityManager()->remove($entity);
        $this->getEntityManager()->flush();
    }

    public function findByModel(string $model): array
    {
        return $this->createQueryBuilder('c')
            ->where('LOWER(c.model) LIKE LOWER(:model) ')
            ->orWhere('LOWER(c.manufacturer) LIKE LOWER(:model) ')
            ->setParameter('model', "%$model%")
            ->getQuery()->getResult();
    }
}
