<?php

namespace App\Repository;

use App\Entity\Part;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Part|null find($id, $lockMode = null, $lockVersion = null)
 * @method Part|null findOneBy(array $criteria, array $orderBy = null)
 * @method Part[]    findAll()
 * @method Part[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PartRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Part::class);
    }

    public function save(Part $part): void
    {
        $this->getEntityManager()->persist($part);
        $this->getEntityManager()->flush();
    }

    public function remove(int $id): void
    {
        $entity = $this->getById($id);

        $this->getEntityManager()->remove($entity);
        $this->getEntityManager()->flush();
    }

    public function getById(int $id): Part
    {
        return $this->find($id);
    }

    public function getAll(): array
    {
        return $this->findAll();
    }

    public function findByName(string $name): array
    {
        return $this->createQueryBuilder('p')
            ->where('LOWER(p.name) LIKE LOWER(:name) ')
            ->setParameter('name', "%$name%")
            ->getQuery()->getResult();
    }
}
