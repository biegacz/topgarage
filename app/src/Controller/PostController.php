<?php

namespace App\Controller;

use App\Message\CreatePost;
use App\Message\DeletePost;
use App\Message\EditPost;
use App\Service\PostQuerySrv;
use App\Util\ErrorUtilTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class PostController extends AbstractController
{
    #[Route('/', name: 'homepage', options: ['expose' => true])]
    public function index(Request $request): Response
    {
        error_log($request->getClientIp());
        return $this->render('post/index.html.twig');
    }

    #[Route('/api/posts', name: 'post_get', methods: ['GET'], options: ['expose' => true])]
    public function getAll(PostQuerySrv $postQuerySrv): Response
    {
        $posts = $postQuerySrv->getAllAsArray();
        return $this->json(['result' => $posts]);
    }

    #[Route('/api/posts', name: 'post_create', methods: ['POST'], options: ['expose' => true]), IsGranted('ROLE_MANAGER')]
    public function createPost(MessageBusInterface $messageBus, Request $request, ValidatorInterface $validator): Response
    {
        $createPost = new CreatePost(
            $request->request->get('title'),
            $request->request->get('content'),
            $this->getUser()->getId()
        );

        $errors = ErrorUtilTrait::getFromViolationList($validator->validate($createPost));
        if (count($errors) > 0) {
            return $this->json($errors, 400);
        }

        $messageBus->dispatch(
            $createPost
        );

        return $this->json(['result' => true]);
    }

    #[Route('/api/posts/{id}/edit', name: 'post_edit', methods: ['POST'], options: ['expose' => true]), IsGranted('ROLE_MANAGER')]
    public function editPost(int $id, MessageBusInterface $messageBus, Request $request, ValidatorInterface $validator): Response
    {
        $editPost = new EditPost(
            $id,
            $request->request->get('title'),
            $request->request->get('content')
        );

        $errors = ErrorUtilTrait::getFromViolationList($validator->validate($editPost));
        if (count($errors) > 0) {
            return $this->json($errors, 400);
        }

        $messageBus->dispatch(
            $editPost
        );

        return $this->json(['result' => true]);
    }

    #[Route('/api/posts/{id}/delete', name: 'post_delete', methods: ['DELETE'], options: ['expose' => true]), IsGranted('ROLE_MANAGER')]
    public function deletePost(int $id, MessageBusInterface $messageBus): Response
    {
        $messageBus->dispatch(
            new DeletePost($id)
        );

        return $this->json(['result' => true]);
    }
}
