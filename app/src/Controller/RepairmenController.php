<?php

namespace App\Controller;

use App\Entity\User;
use App\Service\User\UserFinder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RepairmenController extends AbstractController
{
    #[Route('/repairmen', name: 'repairmen_get', methods: ['GET'], options: ['expose' => true])]
    public function index(UserFinder $userFinder): Response
    {
        $result = $userFinder->getAllByRole(User::ROLE_WORKER);
        return $this->json(['result' => $result]);
    }
}
