<?php

namespace App\Controller;

use App\Message\CreateRepair;
use App\Message\DeleteRepair;
use App\Message\EditRepair;
use App\Service\RepairQuerySrv;
use App\Service\Util\TransactionalSrv;
use App\Util\ErrorUtilTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RepairRegistryController extends AbstractController
{
    #[Route('/repair/registry', name: 'repair_registry_index', options: ['expose' => true]), IsGranted('ROLE_WORKER')]
    public function index(): Response
    {
        return $this->render('repair_registry/index.html.twig');
    }

    #[Route('/api/repairs', name: 'repair_get', methods: ['GET'], options: ['expose' => true]), IsGranted('ROLE_WORKER')]
    public function getAll(RepairQuerySrv $repairQuerySrv, Request $request): Response
    {
        $repairs = $repairQuerySrv->getAllAsArray(
            $request->query->get('q')
        );
        return $this->json(['result' => $repairs]);
    }

    #[Route('/api/repairs',
        name: 'repair_create',
        methods: ['POST'],
        options: ['expose' => true]
    ), IsGranted('ROLE_WORKER')]
    public function createRepair(MessageBusInterface $messageBus, Request $request, ValidatorInterface $validator): Response
    {
        $createRepairMessage = new CreateRepair(
            $request->request->get('description'),
            $request->request->get('carId'),
            $request->request->all('repairmen'),
            $request->request->all('parts'),
        );

        $errors = ErrorUtilTrait::getFromViolationList($validator->validate($createRepairMessage));
        if (count($errors) > 0) {
            return $this->json($errors, 400);
        }

        $messageBus->dispatch($createRepairMessage);

        return $this->json(['result' => true]);
    }

    #[Route('/api/repairs/{id}/edit', name: 'repair_edit', methods: ['POST'], options: ['expose' => true]), IsGranted('ROLE_WORKER')]
    public function editRepair(int $id, MessageBusInterface $messageBus, Request $request, TransactionalSrv $transactionalSrv): Response
    {
        $editRepair = new EditRepair(
            $id,
            $request->request->get('description'),
            $request->request->get('carId'),
            $request->request->all('repairmen'),
            $request->request->all('parts'),
        );

        $transactionalSrv->start();

        $messageBus->dispatch(
            $editRepair
        );

        $transactionalSrv->commit();

        return $this->json(['result' => true]);
    }

    #[ Route('/api/repairs/{id}/delete', name: 'repair_delete', methods: ['DELETE'], options: ['expose' => true]), IsGranted('ROLE_MANAGER')]
    public function deleteRepair(int $id, MessageBusInterface $messageBus): Response
    {
        $messageBus->dispatch(
            new DeleteRepair($id)
        );

        return $this->json(['result' => true]);
    }
}
