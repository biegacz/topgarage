<?php

namespace App\Controller;

use App\Message\CreateVisit;
use App\Message\DeleteVisit;
use App\Message\EditVisit;
use App\Service\VisitQuerySrv;
use App\Util\ErrorUtilTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class VisitController extends AbstractController
{
    #[Route(
        '/api/visits/{clientId}',
        name: 'visit_get',
        methods: ['GET'],
        options: ['expose' => true]
    ), IsGranted('ROLE_MANAGER')]
    public function getAll($clientId, VisitQuerySrv $visitQuerySrv): Response
    {
        $visits = $visitQuerySrv->getAllAsArray($clientId);
        return $this->json(['result' => $visits]);
    }

    #[Route('/api/visits', name: 'visit_create', methods: ['POST'], options: ['expose' => true]), IsGranted('ROLE_MANAGER')]
    public function createVisit(MessageBusInterface $messageBus, Request $request, ValidatorInterface $validator): Response
    {
        $createVisit = new CreateVisit(
            $request->request->get('description'),
            $request->request->get('date'),
            $request->request->get('client'),
        );

        $errors = ErrorUtilTrait::getFromViolationList($validator->validate($createVisit));
        if (count($errors) > 0) {
            return $this->json($errors, 400);
        }

        $messageBus->dispatch(
            $createVisit
        );

        return $this->json(['result' => true]);
    }

    #[Route('/api/visits/{id}/edit', name: 'visit_edit', methods: ['POST'], options: ['expose' => true]), IsGranted('ROLE_MANAGER')]
    public function editVisit(int $id, MessageBusInterface $messageBus, Request $request, ValidatorInterface $validator): Response
    {
        $editVisit = new EditVisit(
            $id,
            $request->request->get('description'),
            $request->request->get('date')
        );

        $errors = ErrorUtilTrait::getFromViolationList($validator->validate($editVisit));
        if (count($errors) > 0) {
            return $this->json($errors, 400);
        }

        $messageBus->dispatch(
            $editVisit
        );

        return $this->json(['result' => true]);
    }

    #[Route('/api/visits/{id}/delete', name: 'visit_delete', methods: ['DELETE'], options: ['expose' => true]), IsGranted('ROLE_MANAGER')]
    public function deleteVisit(int $id, MessageBusInterface $messageBus): Response
    {
        $messageBus->dispatch(
            new DeleteVisit($id)
        );

        return $this->json(['result' => true]);
    }
}
