<?php

namespace App\Controller;

use App\Message\CreateCar;
use App\Message\DeleteCar;
use App\Message\EditCar;
use App\Service\Car\CarQuerySrv;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class CarController extends AbstractController
{
    #[Route('/car', name: 'car_index', options: ['expose' => true])]
    public function index(): Response
    {
        return $this->render('car/index.html.twig');
    }

    #[Route('/api/cars', name: 'car_get', methods: ['GET'], options: ['expose' => true]), IsGranted('ROLE_WORKER')]
    public function getAll(CarQuerySrv $carQuerySrv, Request $request): Response
    {
        $cars = $carQuerySrv->getAllAsArray(
            $request->query->get('q')
        );
        return $this->json(['result' => $cars]);
    }

    #[Route('/api/cars', name: 'car_create', methods: ['POST'], options: ['expose' => true]), IsGranted('ROLE_WORKER')]
    public function createCar(MessageBusInterface $messageBus, Request $request): Response
    {
        $messageBus->dispatch(
            new CreateCar(
                $request->request->get('model'),
                $request->request->get('manufacturer'),
                $request->request->get('vin'),
                $request->request->get('client')
            )
        );

        return $this->json(['result' => true]);
    }

    #[Route('/api/cars/{id}/edit', name: 'car_edit', methods: ['POST'], options: ['expose' => true]), IsGranted('ROLE_WORKER')]
    public function editCar(int $id, MessageBusInterface $messageBus, Request $request): Response
    {
        $messageBus->dispatch(
            new EditCar(
                $id,
                $request->request->get('model'),
                $request->request->get('manufacturer'),
                $request->request->get('vin')
            )
        );

        return $this->json(['result' => true]);
    }

    #[Route('/api/cars/{id}/delete', name: 'car_delete', methods: ['DELETE'], options: ['expose' => true]), IsGranted('ROLE_MANAGER')]
    public function deleteCar(int $id, MessageBusInterface $messageBus): Response
    {
        $messageBus->dispatch(
            new DeleteCar($id)
        );

        return $this->json(['result' => true]);
    }
}
