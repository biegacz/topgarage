<?php

namespace App\Controller;

use App\Message\CreateClient;
use App\Message\DeleteClient;
use App\Message\EditClient;
use App\Service\Client\ClientQuerySrv;
use App\Util\ErrorUtilTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ClientRegistryController extends AbstractController
{
    #[Route('/client/registry', name: 'client_registry_index')]
    public function index(): Response
    {
        return $this->render('client_registry/index.html.twig');
    }

    #[Route('/api/clients', name: 'client_get', methods: ['GET'], options: ['expose' => true]), IsGranted('ROLE_WORKER')]
    public function getAll(ClientQuerySrv $clientQuerySrv, Request $request): Response
    {
        $clients = $clientQuerySrv->getAllAsArray(
            $request->query->get('q')
        );
        return $this->json(['result' => $clients]);
    }

    #[Route('/api/clients', name: 'client_create', methods: ['POST'], options: ['expose' => true]), IsGranted('ROLE_WORKER')]
    public function createClient(MessageBusInterface $messageBus, Request $request, ValidatorInterface $validator): Response
    {
        $createClient = new CreateClient(
            $request->request->get('name'),
            $request->request->get('email'),
            $request->request->get('phone')
        );

        $errors = ErrorUtilTrait::getFromViolationList($validator->validate($createClient));
        if (count($errors) > 0) {
            return $this->json($errors, 400);
        }

        $messageBus->dispatch(
            $createClient
        );

        return $this->json(['result' => true]);
    }

    #[Route('/api/clients/{id}/edit', name: 'client_edit', methods: ['POST'], options: ['expose' => true]), IsGranted('ROLE_WORKER')]
    public function editClient(int $id, MessageBusInterface $messageBus, Request $request, ValidatorInterface $validator): Response
    {
        $editClient = new EditClient(
            $id,
            $request->request->get('name'),
            $request->request->get('email'),
            $request->request->get('phone')
        );

        $errors = ErrorUtilTrait::getFromViolationList($validator->validate($editClient));
        if (count($errors) > 0) {
            return $this->json($errors, 400);
        }

        $messageBus->dispatch(
            $editClient
        );

        return $this->json(['result' => true]);
    }

    #[Route('/api/clients/{id}/delete', name: 'client_delete', methods: ['DELETE'], options: ['expose' => true]), IsGranted('ROLE_MANAGER')]
    public function deleteClient(int $id, MessageBusInterface $messageBus): Response
    {
        $messageBus->dispatch(
            new DeleteClient($id)
        );

        return $this->json(['result' => true]);
    }
}
