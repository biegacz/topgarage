<?php

namespace App\Controller;

use App\Form\ContactType;
use App\Message\SendContactMessage;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends AbstractController
{
    #[Route('/contact', name: 'contact')]
    public function index(Request $request, MessageBusInterface $messageBus): Response
    {
        $form = $this->createForm(ContactType::class);
        $form->handleRequest($request);
        if ($request->isMethod("POST")) {
            $data = $form->getData();
            $messageBus->dispatch(
                new SendContactMessage(
                    $data['name'],
                    $data['email'],
                    $data['message'],
                )
            );

            return $this->redirectToRoute('contact_confirm');
        }

        return $this->render('contact/index.html.twig', [
            'form' => $form->createView()
        ]);
    }

    #[Route('/contact/confirm', name: 'contact_confirm')]
    public function confirm(): Response
    {
        return $this->render('contact/confirm.html.twig');
    }
}
