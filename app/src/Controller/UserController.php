<?php

namespace App\Controller;

use App\Entity\User;
use App\Message\CreateUser;
use App\Message\DeleteUser;
use App\Message\UpdateUser;
use App\Service\User\UserFinder;
use App\Util\ErrorUtilTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserController extends AbstractController
{
    #[Route('/users', name: 'user_index'), IsGranted('ROLE_SUPER_ADMIN')]
    public function index(): Response
    {
        return $this->render('user/index.html.twig',[
            'roles' => User::AVAILABLE_ROLES
        ]);
    }

    #[Route('/api/users', name: 'users_get', methods: ['GET'], options: ['expose' => true]), IsGranted('ROLE_SUPER_ADMIN')]
    public function getUsers(UserFinder $userFinder, Request $request): JsonResponse
    {
        $result = $userFinder->getAllArray(
            $request->query->get('q')
        );
        return $this->json(['result' => $result]);
    }

    #[Route('/api/users', name: 'user_create', methods: ['POST'], options: ['expose' => true]), IsGranted('ROLE_SUPER_ADMIN')]
    public function createUser(Request $request, MessageBusInterface $messageBus, ValidatorInterface $validator): JsonResponse
    {
        $message = new CreateUser(
            $request->request->get('name'),
            $request->request->get('email'),
            $request->request->get('password'),
            $request->request->all('roles')
        );

        $errors = ErrorUtilTrait::getFromViolationList($validator->validate($message));
        if (count($errors) > 0) {
            return $this->json($errors, 400);
        }

        $messageBus->dispatch(
            $message
        );

        return $this->json(['result' => true], Response::HTTP_CREATED);
    }

    #[Route('/api/users/{id}/update', name: 'user_update', methods: ['POST'], options: ['expose' => true]), IsGranted('ROLE_SUPER_ADMIN')]
    public function updateUser(Request $request, $id, MessageBusInterface $messageBus, ValidatorInterface $validator): JsonResponse
    {
        $updateUser = new UpdateUser(
            $id,
            $request->request->get('name'),
            $request->request->get('email'),
            $request->request->all('roles')
        );

        $errors = ErrorUtilTrait::getFromViolationList($validator->validate($updateUser));
        if (count($errors) > 0) {
            return $this->json($errors, 400);
        }

        $messageBus->dispatch(
            $updateUser
        );

        return $this->json(['result' => true], Response::HTTP_CREATED);
    }

    #[Route('/api/users/{id}/delete', name: 'user_delete', methods: ['DELETE'], options: ['expose' => true]), IsGranted('ROLE_SUPER_ADMIN')]
    public function deleteUsers($id, MessageBusInterface $messageBus): JsonResponse
    {
        $messageBus->dispatch(
            new DeleteUser($id)
        );

        return $this->json(['result' => true]);
    }
}
