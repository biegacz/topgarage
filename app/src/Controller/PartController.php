<?php

namespace App\Controller;

use App\Message\CreatePart;
use App\Message\DeletePart;
use App\Message\EditPart;
use App\Service\PartQuerySrv;
use App\Util\ErrorUtilTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class PartController extends AbstractController
{
    #[Route('/part', name: 'part_index')]
    public function index(): Response
    {
        return $this->render('part/index.html.twig');
    }

    #[Route('/api/parts', name: 'part_get', methods: ['GET'], options: ['expose' => true]), IsGranted('ROLE_WORKER')]
    public function getAll(PartQuerySrv $partQuerySrv, Request $request): Response
    {
        $parts = $partQuerySrv->getAllAsArray(
            $request->query->get('q')
        );
        return $this->json(['result' => $parts]);
    }

    #[Route('/api/parts', name: 'part_create', methods: ['POST'], options: ['expose' => true]), IsGranted('ROLE_WORKER')]
    public function createPart(MessageBusInterface $messageBus, Request $request, ValidatorInterface $validator): Response
    {
        $createPart = new CreatePart(
            $request->request->get('name'),
            $request->request->get('manufacturer'),
            $request->request->get('quantity'),
        );

        $errors = ErrorUtilTrait::getFromViolationList($validator->validate($createPart));
        if (count($errors) > 0) {
            return $this->json($errors, 400);
        }

        $messageBus->dispatch(
            $createPart
        );

        return $this->json(['result' => true]);
    }

    #[Route('/api/parts/{id}/edit', name: 'part_edit', methods: ['POST'], options: ['expose' => true]), IsGranted('ROLE_WORKER')]
    public function editPart(int $id, MessageBusInterface $messageBus, Request $request, ValidatorInterface $validator): Response
    {
        $editPart = new EditPart(
            $id,
            $request->request->get('name'),
            $request->request->get('manufacturer'),
            $request->request->get('quantity')
        );

        $errors = ErrorUtilTrait::getFromViolationList($validator->validate($editPart));
        if (count($errors) > 0) {
            return $this->json($errors, 400);
        }

        $messageBus->dispatch(
            $editPart
        );

        return $this->json(['result' => true]);
    }

    #[Route('/api/parts/{id}/delete', name: 'part_delete', methods: ['DELETE'], options: ['expose' => true]), IsGranted('ROLE_MANAGER')]
    public function deletePart(int $id, MessageBusInterface $messageBus): Response
    {
        $messageBus->dispatch(
            new DeletePart($id)
        );

        return $this->json(['result' => true]);
    }
}
