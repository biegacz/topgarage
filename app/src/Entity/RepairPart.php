<?php

namespace App\Entity;

use App\Repository\RepairPartRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RepairPartRepository::class)
 */
class RepairPart
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Part::class, inversedBy="repairParts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $part;

    /**
     * @ORM\ManyToOne(targetEntity=Repair::class, inversedBy="repairParts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $repair;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $createdAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPart(): ?Part
    {
        return $this->part;
    }

    public function setPart(?Part $part): self
    {
        $this->part = $part;

        return $this;
    }

    public function getRepair(): ?Repair
    {
        return $this->repair;
    }

    public function setRepair(?Repair $repair): self
    {
        $this->repair = $repair;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'part' => $this->getPart()->toArray(),
            'quantity' => $this->getQuantity(),
            'createdAt' => $this->getCreatedAt(),
        ];
    }
}
