<?php

namespace App\Entity;

use App\Repository\RepairRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RepairRepository::class)
 */
class Repair
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity=Car::class, inversedBy="repairs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $car;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="repairs")
     */
    private $repairmen;

    /**
     * @ORM\OneToMany(targetEntity=RepairPart::class, mappedBy="repair", cascade={"persist","remove"}, orphanRemoval=true)
     */
    private $repairParts;

    public function __construct()
    {
        $this->repairmen = new ArrayCollection();
        $this->parts = new ArrayCollection();
        $this->repairParts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCar(): ?Car
    {
        return $this->car;
    }

    public function setCar(?Car $car): self
    {
        $this->car = $car;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getRepairmen(): Collection
    {
        return $this->repairmen;
    }

    public function addRepairman(User $repairman): self
    {
        if (!$this->repairmen->contains($repairman)) {
            $this->repairmen[] = $repairman;
        }

        return $this;
    }

    public function removeRepairman(User $repairman): self
    {
        $this->repairmen->removeElement($repairman);

        return $this;
    }

    /**
     * @return Collection|RepairPart[]
     */
    public function getRepairParts(): Collection
    {
        return $this->repairParts;
    }

    public function addRepairPart(RepairPart $repairPart): self
    {
        if (!$this->repairParts->contains($repairPart)) {
            $this->repairParts[] = $repairPart;
            $repairPart->setRepair($this);
        }

        return $this;
    }

    public function removeRepairPart(RepairPart $repairPart): self
    {
        if ($this->repairParts->removeElement($repairPart)) {
            // set the owning side to null (unless already changed)
            if ($repairPart->getRepair() === $this) {
                $repairPart->setRepair(null);
            }
        }

        return $this;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'description' => $this->getDescription(),
            'created_at' => $this->getCreatedAt()->format('Y-m-d H:i:s'),
            'car' => $this->getCar()->toArray(false),
            'repairmen' => $this->getRepairmen()->map(fn(User $user) => $user->toArray()),
            'parts' => $this->getRepairParts()->map(fn(RepairPart $part) => $part->toArray())
        ];
    }

    public function removeRepairmanById(int $id): void
    {
        $filtered = $this->repairmen->filter(function (User $user) use ($id) {
            return $user->getId() === $id;
        });

        foreach ($filtered as $item) {
            $this->repairmen->removeElement($item);
        }
    }
}
