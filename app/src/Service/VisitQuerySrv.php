<?php


namespace App\Service;


use App\Entity\Visit;
use App\Repository\VisitRepository;

class VisitQuerySrv
{
    private VisitRepository $visitRepository;

    public function __construct(VisitRepository $visitRepository)
    {
        $this->visitRepository = $visitRepository;
    }

    public function getAllAsArray(int $clientId): array
    {
        $data = $this->visitRepository->findAllByClient($clientId);
        return array_map(fn(Visit $visit) => $visit->toArray(), $data);
    }
}