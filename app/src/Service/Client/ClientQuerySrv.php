<?php

namespace App\Service\Client;

use App\Entity\Client;
use App\Repository\ClientRepository;

class ClientQuerySrv
{
    private ClientRepository $clientRepository;

    public function __construct(ClientRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    public function getAll(): array
    {
        return $this->clientRepository->getAll();
    }

    public function getAllAsArray(?string $name): array
    {
        $clients = $this->clientRepository->findByName($name ?? '');
        return array_map(fn(Client $client) => $client->toArray(), $clients);
    }
}