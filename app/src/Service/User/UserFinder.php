<?php

namespace App\Service\User;

use App\Entity\User;
use App\Repository\UserRepository;

class UserFinder
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Return all users as array
     */
    public function getAllArray(?string $nameOrEmail): array
    {
        $users = $this->userRepository->findByNameOrEmail($nameOrEmail ?? '');
        return array_map(fn(User $user) => $this->getAsArray($user), $users);
    }

    public function getAllByRole(string $role)
    {
        $users = $this->userRepository->findAllByRole($role);
        return array_map(fn(User $user) => $this->getAsArray($user), $users);
    }

    private function getAsArray(User $user): array
    {
        return [
            'name' => $user->getName(),
            'email' => $user->getEmail(),
            'id' => $user->getId(),
            'roles' => $user->getRoles(),
            'created_at' => $user->getCreatedAt()->format('Y-m-d H:i:s')
        ];
    }
}