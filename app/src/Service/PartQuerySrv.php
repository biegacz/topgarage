<?php

namespace App\Service;

use App\Entity\Part;
use App\Entity\RepairPart;
use App\Repository\PartRepository;
use App\Repository\RepairPartRepository;

class PartQuerySrv
{
    private PartRepository $partRepository;
    private RepairPartRepository $repairPartRepository;

    public function __construct(PartRepository $partRepository, RepairPartRepository $repairPartRepository)
    {
        $this->partRepository = $partRepository;
        $this->repairPartRepository = $repairPartRepository;
    }

    public function getAll(): array
    {
        return $this->partRepository->getAll();
    }

    public function getAllAsArray(?string $name): array
    {
        $parts = $this->partRepository->findByName($name ?? '');
        return array_map(function (Part $part) {
            $data = $part->toArray();
            $data['available_quantity'] = $part->getQuantity() - $this->getPartUsage($part);
            return $data;
        }, $parts);
    }

    public function getPartUsage(Part $part): int
    {
        $repairParts = $this->repairPartRepository->findBy(['part' => $part]);
        return array_reduce($repairParts, function ($total, RepairPart $repairPart) {
            return $total + $repairPart->getQuantity();
        }, 0);
    }
}