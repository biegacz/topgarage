<?php

namespace App\Service\Util;

use Doctrine\ORM\EntityManagerInterface;

class TransactionalSrv
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function start()
    {
        $this->entityManager->beginTransaction();
    }

    public function commit()
    {
        $this->entityManager->commit();
    }

    public function rollback()
    {
        $this->entityManager->rollback();
    }

    public function transactional(callable $callback)
    {
        $this->entityManager->transactional($callback);
    }
}