<?php

namespace App\Service;

use App\Entity\Repair;
use App\Repository\RepairRepository;

class RepairQuerySrv
{
    private RepairRepository $repairRepository;

    public function __construct(RepairRepository $repairRepository)
    {
        $this->repairRepository = $repairRepository;
    }

    public function getAll(): array
    {
        return $this->repairRepository->getAll();
    }

    public function getAllAsArray(?string $description): array
    {
        $repairs = $this->repairRepository->findByDescription($description ?? '');
        return array_map(fn(Repair $repair) => $repair->toArray(), $repairs);
    }
}