<?php

namespace App\Service;

use App\Entity\Post;
use App\Repository\PostRepository;

class PostQuerySrv
{
    private PostRepository $postRepository;

    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    public function getAll(): array
    {
        return $this->postRepository->getAll();
    }

    public function getAllAsArray(): array
    {
        $posts = $this->getAll();
        return array_map(fn(Post $post) => $post->toArray(), $posts);
    }

}