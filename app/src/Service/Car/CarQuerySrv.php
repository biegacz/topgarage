<?php

namespace App\Service\Car;

use App\Entity\Car;
use App\Repository\CarRepository;

class CarQuerySrv
{
    private CarRepository $carRepository;

    public function __construct(CarRepository $carRepository)
    {
        $this->carRepository = $carRepository;
    }

    public function getAll(): array
    {
        return $this->carRepository->getAll();
    }

    public function getAllAsArray(?string $model): array
    {
        $cars = $this->carRepository->findByModel($model ?? '');
        return array_map(fn(Car $car) => $car->toArray(), $cars);
    }
}